import React from 'react';

const MainList = (props) => {
  return (
    <div className="bg">
      <div className="display">
        <div className="main-auth">
          <button className="sign-in" onClick={props.handleClickSignIn}>SignIn</button>
          <button className="sign-up" onClick={props.handleClickSignUp}>SignUp</button>
        </div>
      </div>
    </div>);
};

export default MainList;