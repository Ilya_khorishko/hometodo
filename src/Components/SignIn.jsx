import React from 'react';
import axios from 'axios'

class SignIn extends React.Component {
  state = {
    first_name: '',
    last_name: '',
    e_mail: '',
    password: ''
  };

  heandleChangeSignInEmail = (e) => {
    this.setState({e_mail: e.target.value})
  };

  heandleChangeSignInPassword = (e) => {
    this.setState({password: e.target.value})
  };

  handleClickSignIn = async (e) => {
    e.preventDefault();
    const data = this.state;
    try {
      const result = await axios.post(`${process.env.REACT_APP_API_URL}/v1/signIn`, data);
      localStorage.setItem('token', result.data)
      this.props.switchState();
    } catch (err) {
      console.log(err);
    }
  }
  //прочекать без коллбека
  render() {
    return (
      <>
        <div className="bg">
          <div className="display">
            <div className="sign-up">
              <h1>SignIn</h1>
              <br />
              <form className="form" onSubmit={() => this.props.handleSumbitSignIn}>
                <p>e-mail:</p><input type="text"
                  onChange={this.heandleChangeSignInEmail} className="first-name" />
                <p>password:</p><input type="text"
                  onChange={this.heandleChangeSignInPassword} className="first-name" />
              </form>
              <button onClick={this.handleClickSignIn}>sucessful</button>
            </div>
          </div>
        </div>
      </>
    );
  };
}

export default SignIn;